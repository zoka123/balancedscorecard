<?php

namespace Zantolov\BsBundle\Enum;

use Zantolov\AppBundle\Enum\BaseEnum;

class ObjectivePerspectiveEnum extends BaseEnum
{
    const PERSPECTIVE_FINANCIAL = 1;
    const PERSPECTIVE_CUSTOMER = 2;
    const PERSPECTIVE_INTERNAL = 3;
    const LEARNING_GROWTH = 4;

    public static function getColorByPerspective()
    {


    }

}