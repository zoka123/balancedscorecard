<?php

class LoadPerspectiveData extends \Zantolov\AppBundle\DataFixtures\ORM\AbstractDbFixture
{

    public function load(\Doctrine\Common\Persistence\ObjectManager $manager)
    {

        $data = [
            'Financije'           => '#A1D490',
            'Kupci'           => '#BD4A91',
            'Procesi' => '#FFD485',
            'Učenje i razvoj'  => '#75C5FA',
        ];

        $i = 1;
        foreach ($data as $name => $color) {
            $p = new \Zantolov\BsBundle\Entity\Perspective();
            $p->setColor($color);
            $p->setTitle($name);
            $manager->persist($p);
            $this->setReference('p' . substr($name, 0, 1), $p);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }


}
