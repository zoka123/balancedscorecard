<?php

class LoadObjectiveData extends \Zantolov\AppBundle\DataFixtures\ORM\AbstractDbFixture
{

    public function load(\Doctrine\Common\Persistence\ObjectManager $manager)
    {
//        return;

        $data = [
            ['Povećati profit', 'C1', 'F'],
            ['Povećati prihode', 'C2', 'F'],
            ['Smanjiti rashode', 'C3', 'F'],

            ['Povećati cijene novih proizvoda', 'C5', 'K'],
            ['Poboljšati servis', 'C6', 'K'],
            ['Povećati opseg plasmana', 'C4', 'K'],
            ['Pojačati ponudu standardnih proizvoda', 'C7', 'K'],

            ['Razviti nove proizvode', 'C8', 'P'],
            ['Poboljšati planiranje potražnje', 'C9', 'P'],
            ['Skratiti vrijeme isporuke', 'C10', 'P'],
            ['Smanjiti zalihe', 'C11', 'P'],

            ['Obrazovati se za nove tehnologije', 'C12', 'U'],
            ['Pokrenuti procese inovacija', 'C13', 'U'],
            ['Obrazovati za upravljanje proizvodnjom', 'C14', 'U'],
        ];

        foreach ($data as $obj) {
            $o = new \Zantolov\BsBundle\Entity\Objective();
            $o->setTitle($obj[0]);
            $o->setCode($obj[1]);
            $o->setPerspective($this->getReference('p' . $obj[2]));
            $manager->persist($o);
            $this->setReference( $obj[1], $o);
        }


        $hierarchy = [
            1  => [2, 3],
            2  => [4, 5],
            3  => [7, 10, 11],
            4  => [6, 7, 10],
            5  => [8],
            8  => [12, 13],
            10 => [9, 14],
            11 => [14]
        ];


        foreach ($hierarchy as $parent => $children) {
            /** @var \Zantolov\BsBundle\Entity\Objective $parent */
            $parent = $this->getReference('C' . $parent);
            foreach ($children as $c) {
                $parent->addChild($this->getReference('C' . $c));
            }
        }

        $manager->flush();
    }


    public function getOrder()
    {
        return 2;
    }
}
