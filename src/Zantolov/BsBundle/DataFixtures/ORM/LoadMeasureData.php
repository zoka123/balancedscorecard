<?php

class LoadMeasureData extends \Zantolov\AppBundle\DataFixtures\ORM\AbstractDbFixture
{

    public function load(\Doctrine\Common\Persistence\ObjectManager $manager)
    {
//        return;

        $data = [
            ['Udjel profita u ukupnom prihodu', 'M1', 15, 8, 5, 0, 'C1'],
            ['Iznos prihoda (kn)', 'M2', 1500, 1000, 800, 600, 'C2'],
            ['Iznos rashoda (kn)', 'M3', 1200, 850, 700, 550, 'C3'],
            ['Broj prodanih proizvoda (kom)', 'M4', 150, 100, 80, 60, 'C4'],
            ['Povećanje cijena novih proizvoda (%)', 'M5', 10, 6, 3, 0, 'C5'],
            ['Prosječno trajanje popravka (dan)', 'M6', 1, 5, 10, 14, 'C6'],
            ['Udjel standardnih proizvoda u vrijednosti svih ponuda (%)', 'M7', 70, 60, 50, 40, 'C7'],
            ['Udjel novih proizvoda u katalogu (%)', 'M8', 30, 20, 10, 0, 'C8'],
            ['Stand. devijacija kašnjenja', 'M9', 0, 1, 2, 4, 'C9'],
            ['Prosječno kašnjenje (dan)', 'M10', 0, 7, 14, 21, 'C9'],
            ['Prosječno vrijeme isporuke (dan)', 'M11', 40, 50, 70, 80, 'C10'],
            ['Koeficijent obrtaja', 'M12', 8, 7, 4, 2, 'C11'],
            ['Obrazovanje za projektante (sat)', 'M13', 40, 30, 25, 20, 'C12'],
            ['Broj objavljenih stručnih radova (kom)', 'M15', 8, 4, 2, 0, 'C13'],
            ['Broj patenata (kom)', 'M14', 8, 5, 3, 0, 'C13'],
            ['Obrazovanje za referente pripreme rada (sat)', 'M16', 40, 30, 25, 20, 'C14'],
        ];

        foreach ($data as $meas) {
            $m = new \Zantolov\BsBundle\Entity\Measure();
            $m->setTitle($meas[0]);
            $m->setCode($meas[1]);
            $m->D = ($meas[2]);
            $m->dd = ($meas[3]);
            $m->gg = ($meas[4]);
            $m->G = ($meas[5]);
            $m->setObjective($this->getReference($meas[6]));

            $manager->persist($m);
            $this->setReference($m->getCode(), $m);
        }


        $createRatio = function ($parent, $child, $ratioM) use ($manager) {
            $parent = $this->getReference($parent);
            $child = $this->getReference($child);
            $ratio = new \Zantolov\BsBundle\Entity\MeasureRatio();
            $ratio->setMeasure($child);
            $ratio->setParentMeasure($parent);
            $ratio->setRatio($ratioM);
            $manager->persist($ratio);
            return $ratio;
        };

        $createRatio('M1', 'M2', 0.6);
        $createRatio('M1', 'M3', 0.4);
        $createRatio('M2', 'M4', 0.7);
        $createRatio('M2', 'M5', 0.3);
        $createRatio('M3', 'M7', 0.3);
        $createRatio('M3', 'M11', 0.2);
        $createRatio('M3', 'M12', 0.5);
        $createRatio('M4', 'M7', 0.1);
        $createRatio('M4', 'M11', 0.5);
        $createRatio('M4', 'M6', 0.4);
        $createRatio('M5', 'M8', 1);
        $createRatio('M8', 'M13', 0.6);
        $createRatio('M8', 'M15', 0.2);
        $createRatio('M8', 'M14', 0.2);
        $createRatio('M11', 'M9', 0.4);
        $createRatio('M11', 'M10', 0.4);
        $createRatio('M11', 'M16', 0.2);
        $createRatio('M12', 'M16', 1);


        $manager->flush();

    }


    public function getOrder()
    {
        return 3;
    }
}
