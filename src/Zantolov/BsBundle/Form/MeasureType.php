<?php

namespace Zantolov\BsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MeasureType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('code')
            ->add('title')
            ->add('objective', null, ['attr' => ['class' => 'noSelect2']])
            ->add('G')
            ->add('gg', null, ['label' => 'g'])
            ->add('dd', null, ['label' => 'd'])
            ->add('D');
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Zantolov\BsBundle\Entity\Measure'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'zantolov_bsbundle_measure';
    }
}
