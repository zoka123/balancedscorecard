<?php

namespace Zantolov\BsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MeasureRatioType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('parentMeasure')
            ->add('measure')
            ->add('ratio');
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Zantolov\BsBundle\Entity\MeasureRatio'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'zantolov_bsbundle_childmeasure';
    }
}
