<?php

namespace Zantolov\BsBundle\Entity;

use Zantolov\AppBundle\Entity\Traits\BasicEntityTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks
 */
class MeasureRatio implements \JsonSerializable
{
    use BasicEntityTrait;

    /**
     * @ORM\ManyToOne(targetEntity="Measure", inversedBy="childrenMeasures")
     * @ORM\JoinColumn(name="parent_measure_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $parentMeasure;

    /**
     * @ORM\ManyToOne(targetEntity="Measure")
     * @ORM\JoinColumn(name="measure_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $measure;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $ratio = 1;

    /**
     * @return mixed
     */
    public function getMeasure()
    {
        return $this->measure;
    }

    /**
     * @param mixed $measure
     */
    public function setMeasure($measure)
    {
        $this->measure = $measure;
    }


    /**
     * @return string
     */
    public function getRatio()
    {
        return $this->ratio;
    }

    /**
     * @param string $ratio
     */
    public function setRatio($ratio)
    {
        $this->ratio = $ratio;
    }

    /**
     * @return mixed
     */
    public function getParentMeasure()
    {
        return $this->parentMeasure;
    }

    /**
     * @param mixed $parentMeasure
     */
    public function setParentMeasure($parentMeasure)
    {
        $this->parentMeasure = $parentMeasure;
    }

    function jsonSerialize()
    {
        return [
            'measure' => $this->getMeasure(),
            'ratio'   => $this->getRatio(),
            'id'      => $this->getId(),
        ];
    }


}

