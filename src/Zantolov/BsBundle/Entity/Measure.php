<?php

namespace Zantolov\BsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Zantolov\AppBundle\Entity\Traits\BasicEntityTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks
 */
class Measure implements \JsonSerializable
{
    use BasicEntityTrait;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $code;


    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    public $dd;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    public $D;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    public $gg;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    public $G;

    /**
     * @ORM\OneToMany(targetEntity="MeasureRatio", mappedBy="parentMeasure")
     */
    private $childrenMeasures;

    /**
     * @ORM\ManyToOne(targetEntity="Objective", inversedBy="measures")
     * @ORM\JoinColumn(name="objective_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $objective;

    public function __construct()
    {
        $this->childrenMeasures = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }


    public function __toString()
    {
        return $this->code . ' ' . $this->title;
    }

    function jsonSerialize()
    {
        return [
            'id'          => $this->getId(),
            'title'       => $this->getTitle(),
            'code'        => $this->getCode(),
            'objective'   => $this->getObjectiveCode(),
            'perspective' => $this->getPerspectiveId(),
            'children'    => $this->getChildrenList(),
            'D'           => $this->D,
            'dd'          => $this->dd,
            'gg'          => $this->gg,
            'G'           => $this->G,
        ];
    }

    /**
     * @return ArrayCollection
     */
    public function getChildrenMeasures()
    {
        return $this->childrenMeasures;
    }

    /**
     * @param mixed $childrenMeasures
     */
    public function addChildrenMeasure($childrenMeasures)
    {
        $this->childrenMeasures->add($childrenMeasures);
    }

    /**
     * @param mixed $childrenMeasures
     */
    public function removeChildrenMeasure($childrenMeasures)
    {
        $this->childrenMeasures->removeElement($childrenMeasures);
    }

    public function getObjectiveCode()
    {
        if ($this->getObjective()) {
            return $this->getObjective()->getCode();
        }

        return null;
    }

    /**
     * @return mixed
     */
    public function getObjective()
    {
        return $this->objective;
    }

    /**
     * @param mixed $objective
     */
    public function setObjective($objective)
    {
        $this->objective = $objective;
    }

    public function getPerspectiveId()
    {
        if ($this->getObjective()) {
            return $this->getObjective()->getPerspective()->getId();
        }

        return null;
    }

    /**
     * @return null|Perspective
     */
    public function getPerspective()
    {
        if ($this->getObjective()) {
            return $this->getObjective()->getPerspective();
        }

        return null;
    }

    /**
     * @return array
     */
    public function getChildrenList()
    {
        $buf = [];
        /** @var MeasureRatio $m */
        foreach ($this->getChildrenMeasures() as $m) {
            $buf[] = $m;
        }
        return $buf;
    }

    /**
     * @return array
     */
    public function getChildrenMeasuresList()
    {
        $buf = [];
        /** @var MeasureRatio $m */
        foreach ($this->getChildrenMeasures() as $m) {
            $buf[] = $m->getMeasure();
        }
        return $buf;
    }

    /**
     * @param $values
     * @return string
     */
    public function calculate($values)
    {
        if ($this->getChildrenMeasures()->isEmpty()) {
            return $values[$this->id];
        }

        $childrenSum = 0;
        /** @var MeasureRatio $m */
        foreach ($this->getChildrenMeasures() as $m) {
            $value = $m->getMeasure()->calculate($values);
            $childrenSum += $m->getRatio() * (($value - $m->getMeasure()->D) / ($m->getMeasure()->G - $m->getMeasure()->D));
        }

        $result = $this->D + ($this->G - $this->D) * $childrenSum;
        return $result;
    }

}
