<?php

namespace Zantolov\BsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Zantolov\AppBundle\Entity\Traits\BasicEntityTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks
 */
class Perspective implements \JsonSerializable
{
    use BasicEntityTrait;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $color;


    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Objective", mappedBy="perspective")
     */
    private $objectives;

    /**
     * Objective constructor.
     * @param string $title
     */
    public function __construct()
    {
        $this->objectives = new ArrayCollection();
    }


    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param string $color
     */
    public function setColor($color)
    {
        $this->color = $color;
    }


    public function __toString()
    {
        return $this->getTitle();
    }

    /**
     * @return ArrayCollection
     */
    public function getObjectives()
    {
        return $this->objectives;
    }

    /**
     * @param ArrayCollection $objectives
     */
    public function setObjectives($objectives)
    {
        $this->objectives = $objectives;
    }


    function jsonSerialize()
    {
        return [
            'id'    => $this->getId(),
            'title' => $this->getTitle(),
            'color' => $this->getColor(),
        ];
    }

}
