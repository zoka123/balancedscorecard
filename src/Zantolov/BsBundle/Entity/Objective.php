<?php

namespace Zantolov\BsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Zantolov\AppBundle\Entity\Traits\BasicEntityTrait;

/**
 * @ORM\Entity ()
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks
 */
class Objective implements \JsonSerializable
{
    use BasicEntityTrait;
    use TimestampableEntity;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $code;

    /**
     * @ORM\ManyToOne(targetEntity="Perspective", inversedBy="objectives")
     * @ORM\JoinColumn(name="perspective_id", referencedColumnName="id", nullable=true)
     */
    private $perspective;


    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Measure", mappedBy="objective")
     */
    private $measures;

    /**
     * @ORM\ManyToMany(targetEntity="Objective")
     */
    private $children;

    /**
     * Objective constructor.
     * @param string $title
     */
    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->measures = new ArrayCollection();

    }


    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }


    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getPerspective()
    {
        return $this->perspective;
    }

    /**
     * @param mixed $perspective
     */
    public function setPerspective($perspective)
    {
        $this->perspective = $perspective;
    }

    /**
     * @return mixed
     */
    public function getMeasures()
    {
        return $this->measures;
    }

    /**
     * @param mixed $measure
     */
    public function addMeasure($measure)
    {
        $this->measures->add($measure);
    }

    /**
     * @param mixed $measure
     */
    public function removeMeasure($measure)
    {
        $this->measures->removeElement($measure);
    }


    public function __toString()
    {
        return $this->code . ' ' . $this->title;
    }


    function jsonSerialize()
    {
        return [
            'id'          => $this->getId(),
            'title'       => $this->getTitle(),
            'code'        => $this->getCode(),
            'perspective' => $this->getPerspectiveId(),
            'children'    => $this->getChildrenCodes(),
        ];
    }

    /**
     * @return mixed
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param $children
     */
    public function addChild($children)
    {
        $this->children->add($children);
    }

    /**
     * @param $children
     */
    public function removeChild($children)
    {
        $this->children->removeElement($children);
    }

    public function getPerspectiveId()
    {
        if ($this->getPerspective()) {
            return $this->getPerspective()->getId();
        }
        return null;
    }

    public function getChildrenCodes()
    {
        $buff = [];
        foreach ($this->getChildren() as $c) {
            $buff[] = $c->getCode();
        }
        return $buff;
    }

}
