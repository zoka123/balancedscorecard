<?php

namespace Zantolov\BsBundle\Service;

use Doctrine\ORM\QueryBuilder;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class BsService implements ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * BsService constructor.
     * @param $container
     */
    public function __construct($container)
    {
        $this->container = $container;
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function getMeasuresToPopulate()
    {
        //SELECT m.* FROM measure m WHERE m.id NOT IN (SELECT DISTINCT parent_measure_id from measure_ratio)
        $stmt = $this->container->get('doctrine')->getManager()->getConnection()->prepare("SELECT m.* FROM measure m WHERE m.id NOT IN (SELECT DISTINCT parent_measure_id from measure_ratio) ORDER BY CAST(m.code as SIGNED INTEGER) ASC");
        $stmt->execute();
        return $stmt->fetchAll();
    }


}