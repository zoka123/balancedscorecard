<?php

namespace Zantolov\BsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Zantolov\AppBundle\Controller\Traits\EasyControllerTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Zantolov\AppBundle\Enum\ApiResponseEnum;
use Zantolov\BsBundle\Entity\Objective;

/**
 * Class ObjectiveTree
 * @Route("/objective/tree")
 */
class ObjectiveTreeController extends Controller
{
    use EasyControllerTrait;

    /**
     * @Template()
     * @Route("/", name="objective.tree.index")
     * @return array
     */
    public function indexAction()
    {
        $objectives = $this->getRepository('ZantolovBsBundle:Objective')->findAll();

        return compact('objectives');
    }

    /**
     * @Template()
     * @Route("/update", name="objective.tree.update")
     * @Method("POST")
     * @return array
     */
    public function updateAction(Request $request)
    {
        $parent = $request->get('parentId');
        $child = $request->get('childId');

        if (empty($child)) {
            return new JsonResponse(["error"]);
        }


        $repo = $this->getRepository('ZantolovBsBundle:Objective');

        if (empty($parent)) {
            $parent = null;
        } else {
            $parent = $repo->find($parent);
        }

        $parent->addChild($child);
        $this->getManager()->flush();

        return new JsonResponse([
            ApiResponseEnum::API_RESPONSE_KEY_SUCCESS => true
        ]);
    }

    /**
     * @Route("/create", name="objective.tree.create")
     * @Method("POST")
     */
    public function createAction(Request $request)
    {
        $entity = new Objective();
        $entity->setTitle($request->get('title'));
        $entity->setCode($request->get('code'));

        $errors = $this->get('validator')->validate($entity);

        if (count($errors) > 0) {
            $errorsString = (string)$errors;

            return new JsonResponse([
                ApiResponseEnum::API_RESPONSE_KEY_SUCCESS => false,
                ApiResponseEnum::API_RESPONSE_KEY_ERROR   => $errorsString
            ]);
        } else {
            $this->getManager()->persist($entity);
            $this->getManager()->flush();

            return new JsonResponse([
                ApiResponseEnum::API_RESPONSE_KEY_SUCCESS => true,
                ApiResponseEnum::API_RESPONSE_KEY_DATA    => $entity
            ]);
        }
    }


    /**
     * @Route("/delete", name="objective.tree.delete")
     * @Method("POST")
     */
    public function deleteAction(Request $request)
    {
        /** @var Objective $entity */
        $entity = $this->getRepository('ZantolovBsBundle:Objective')->find($request->get('id'));
        if (!empty($entity)) {
            $this->getManager()->remove($entity);
            $this->getManager()->flush();

            return new JsonResponse([
                ApiResponseEnum::API_RESPONSE_KEY_SUCCESS => true,
                ApiResponseEnum::API_RESPONSE_KEY_DATA    => ['id' => $request->get('id')]
            ]);
        } else {
            return new JsonResponse([
                ApiResponseEnum::API_RESPONSE_KEY_SUCCESS => false,
                ApiResponseEnum::API_RESPONSE_KEY_ERROR   => 'Entity not found'
            ]);
        }
    }

}
