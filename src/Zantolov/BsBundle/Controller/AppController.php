<?php

namespace Zantolov\BsBundle\Controller;

use Doctrine\ORM\EntityNotFoundException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Zantolov\BsBundle\Entity\Measure;
use Zantolov\BsBundle\Entity\MeasureRatio;
use Zantolov\BsBundle\Entity\Objective;
use Zantolov\BsBundle\Form\MeasureType;
use Zantolov\BsBundle\Form\ObjectiveType;
use Zantolov\BsBundle\Service\BsService;

class AppController extends Controller
{
    /**
     * @Route("/", name="app.r")
     * @Template()
     */
    public function indexAction()
    {
        /** @var BsService $service */
        $service = $this->get('zantolov.bs');

        $measures = $service->getMeasuresToPopulate();
        $allMeasures = $this->getDoctrine()->getManager()->getRepository('ZantolovBsBundle:Measure')->findAll();

        return compact('measures', 'allMeasures');
    }

    /**
     * @Route("/api/calc", name="api.calc")
     */
    public function apiCalcAction(Request $request)
    {
        $values = $request->get('data');
        $targetMeasureId = $request->get('target');
        /** @var Measure $measure */
        $measure = $this->container->get('doctrine')->getRepository('ZantolovBsBundle:Measure')->find($targetMeasureId);

        $getColor = function ($value, $measure) {
            $red = '#F00202';
            $yellow = '#FFFF00';
            $green = '#7BED00';

            $d = floatval($measure->dd);
            $g = floatval($measure->gg);
            $D = floatval($measure->D);
            $G = floatval($measure->G);

            if (($value >= $g && $value <= $G) || ($value <= $g && $value >= $G)) {
                return $green;
            }

            if (($value < $g && $value > $d) || ($value > $g && $value < $d)) {
                return $yellow;
            }

            if (($value >= $d && $value <= $D) || ($value <= $d && $value >= $D)) {
                return $red;
            }

            return $red;
        };

        $objData = [];
        $objectives = $this->container->get('doctrine')->getRepository('ZantolovBsBundle:Objective')->findAll();
        /** @var Objective $o */
        foreach ($objectives as $o) {
            $dataItem = ['objective' => $o, 'data' => []];
            $objMeasures = $o->getMeasures();
            /** @var Measure $objM */
            foreach ($objMeasures as $objM) {
                foreach ($values as $interval => $val) {
                    $value = floatval(number_format($objM->calculate($val), 4));
                    $dataItem['data'][] = [
                        'measure' => $objM,
                        'value' => $value,
                        'color' => $getColor($value, $objM),
                        'period' => $interval,
                    ];
                }
            }
            $objData[] = $dataItem;
        }


        if (empty($measure)) {
            return new JsonResponse(['status' => 0]);
        }

        /** @var BsService $service */
        $service = $this->get('zantolov.bs');

        $data = [];
        $intervals = [];

        foreach ($values as $interval => $val) {
            $intervals[] = $interval;
            $value = floatval(number_format($measure->calculate($val), 4));
            $data[] = ['y' => $value, 'color' => $getColor($value, $measure)];
        }

        $series = [[
            'name' => $measure->getCode() . "({$measure->D}, {$measure->dd}, {$measure->gg}, {$measure->G})",
            'data' => $data,
        ]];

        return new JsonResponse([
            'status'    => 1,
            'data'      => $series,
            'measure'   => $measure,
            'intervals' => $intervals,
            'objectiveData' => $objData,

        ]);
    }

    /**
     * @Route("/map", name="objective.map")
     * @Template()
     */
    public function mapUrlAction()
    {
        $objectives = $this->getDoctrine()->getRepository('ZantolovBsBundle:Objective')->findAll();

        $perspectiveGroups = [];
        /** @var Objective $objective */
        foreach ($objectives as $objective) {
            if (!$objective->getPerspective()) {
                continue;
            }

            $key = $objective->getPerspective()->getId();
            if (!isset($perspectiveGroups[$key])) {
                $perspectiveGroups[$key] = "";
            }
            $perspectiveGroups[$key] .= " " . $objective->getCode();
        }

        $imgString = 'http://g.gravizo.com/g?';

        $pairs = "";
        foreach ($objectives as $objective) {
            foreach ($objective->getChildren() as $child) {
                $pairs .= $child->getCode() . "->" . $objective->getCode() . ";\n";
            }
        }

        $pgStr = "";
        foreach ($perspectiveGroups as $pg) {
            $pgStr .= "{rank=same; {$pg}}\n";
        }

        $imgString .= "
   digraph G {
   rankdir = BT
   {$pairs}

   {$pgStr}

 }
";

        return new Response($imgString . '&t=' . time());
    }

    /**
     * @Route("/mmap", name="measure.map")
     * @Template()
     */
    public function measureMapUrlAction()
    {
        $measures = $this->getDoctrine()->getRepository('ZantolovBsBundle:Measure')->findAll();
        $measureRatios = $this->getDoctrine()->getRepository('ZantolovBsBundle:MeasureRatio')->findAll();

        $perspectiveGroups = [];
        /** @var Measure $m */
        foreach ($measures as $m) {
            if (!$m->getPerspective()) {
                continue;
            }

            $key = $m->getPerspective()->getId();
            if (!isset($perspectiveGroups[$key])) {
                $perspectiveGroups[$key] = "";
            }
            $perspectiveGroups[$key] .= " " . $m->getCode();
        }

        $imgString = 'http://g.gravizo.com/g?';

        $pairs = "";
        /** @var MeasureRatio $mr */
        foreach ($measureRatios as $mr) {
            $label = "[style=bold,label=\"{$mr->getRatio()}\"]";
            $pairs .= $mr->getMeasure()->getCode() . "->" . $mr->getParentMeasure()->getCode() . ";\n";
        }

        $pgString = "";
        foreach ($perspectiveGroups as $pg) {
            $pgString .= "{rank=same; {$pg}}\n";
        }

        $imgString .= "
   digraph G {
   rankdir = BT
   {$pairs}

   {$pgString}

 }
";

        return new Response($imgString . '&t=' . time());
    }

    /**
     * @Route("/app/o", name="app.o")
     * @Template()
     */
    public function objectivesAction()
    {
        $objectives = $this->getDoctrine()->getRepository('ZantolovBsBundle:Objective')->findAll();
        $perspectives = $this->getDoctrine()->getRepository('ZantolovBsBundle:Perspective')->findAll();
        $objectives = json_encode($objectives);

        return compact('objectives', 'perspectives');
    }

    /**
     * @Route("/app/m", name="app.m")
     * @Template()
     */
    public function measuresAction()
    {
        $measures = $this->getDoctrine()->getRepository('ZantolovBsBundle:Objective')->findAll();
        $perspectives = $this->getDoctrine()->getRepository('ZantolovBsBundle:Perspective')->findAll();
        $ratios = $this->getDoctrine()->getRepository('ZantolovBsBundle:MeasureRatio')->findAll();

        return compact('measures', 'ratios', 'perspectives');
    }

    /**
     * @Route("/api/objectives", name="api.objectives")
     */
    public function objectivesApiAction()
    {
        $objectives = $this->getDoctrine()->getRepository('ZantolovBsBundle:Objective')->findAll();
        return new JsonResponse($objectives);
    }


    /**
     * @Route("/api/measures", name="api.measures")
     */
    public function measuresApiAction()
    {
        $m = $this->getDoctrine()->getRepository('ZantolovBsBundle:Measure')->findAll();
        return new JsonResponse($m);
    }

    /**
     * @Route("/api/o/{id}", name="api.objectives.edit")
     * @Template()
     */
    public function objectiveEditAction(Request $request, $id = null)
    {
        $o = $this->getDoctrine()->getRepository('ZantolovBsBundle:Objective')->find($id);
        if (empty($o)) {
            $o = new Objective();
        }

        $perspective = $request->get('perspective');
        if (!empty($perspective)) {
            $perspective = $this->getDoctrine()->getRepository('ZantolovBsBundle:Perspective')->find((int)$perspective);
            if (!empty($perspective)) {
                $o->setPerspective($perspective);
            }
        }

        $form = $this->createForm(
            new ObjectiveType(),
            $o,
            [
                'action' => $this->generateUrl('api.objectives.edit', ['id' => $id]),
                'method' => 'POST',
            ]
        );

        $form->add('submit', 'submit', [
                'label' => 'Save',
                'attr'  => [
                    'class' => 'btn btn-success btn-lg'
                ]
            ]
        )->handleRequest($request);

        if ($request->getMethod() == 'POST') {
            if ($form->isValid()) {
                $this->getDoctrine()->getManager()->persist($o);
                $this->getDoctrine()->getManager()->flush();
                return new Response('<script>parent.done()</script>');
            }
        }

        $form = $form->createView();

        return compact('form');

    }

    /**
     * @Route("/api/o-remove/{id}", name="api.objectives.remove")
     */
    public function objectiveRemoveAction(Request $request, $id = null)
    {
        $o = $this->getDoctrine()->getRepository('ZantolovBsBundle:Objective')->find($id);
        if (empty($o)) {
            return new JsonResponse(['status' => 0]);
        }

        $this->getDoctrine()->getManager()->remove($o);
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(['status' => 1]);
    }

    /**
     * @Route("/api/m/{id}", name="api.measures.edit")
     * @Template()
     */
    public function measureEditAction(Request $request, $id = null)
    {
        $m = $this->getDoctrine()->getRepository('ZantolovBsBundle:Measure')->find($id);
        if (empty($m)) {
            $m = new Measure();
        }

        $obj = $request->get('objective');
        if (!empty($obj)) {
            $obj = $this->getDoctrine()->getRepository('ZantolovBsBundle:Objective')->find((int)$obj);
            if (!empty($obj)) {
                $m->setObjective($obj);
            }
        }

        $form = $this->createForm(
            new MeasureType(),
            $m,
            [
                'action' => $this->generateUrl('api.measures.edit', ['id' => $id]),
                'method' => 'POST',
            ]
        );

        $form->add('submit', 'submit', [
                'label' => 'Save',
                'attr'  => [
                    'class' => 'btn btn-success btn-lg'
                ]
            ]
        )->handleRequest($request);

        if ($request->getMethod() == 'POST') {
            if ($form->isValid()) {
                $this->getDoctrine()->getManager()->persist($m);
                $this->getDoctrine()->getManager()->flush();
                return new Response('<script>parent.done()</script>');
            }
        }

        $form = $form->createView();

        return compact('form');

    }

    /**
     * @Route("/api/m-remove/{id}", name="api.measures.remove")
     */
    public function measureRemoveAction(Request $request, $id = null)
    {
        $m = $this->getDoctrine()->getRepository('ZantolovBsBundle:Measure')->find($id);
        if (empty($m)) {
            return new JsonResponse(['status' => 0]);
        }

        $this->getDoctrine()->getManager()->remove($m);
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(['status' => 1]);
    }

    /**
     * @Route("/api/r-remove/{id}", name="api.ratio.remove")
     */
    public function ratioRemoveAction(Request $request, $id = null)
    {
        $m = $this->getDoctrine()->getRepository('ZantolovBsBundle:MeasureRatio')->find($id);
        if (empty($m)) {
            return new JsonResponse(['status' => 0]);
        }

        $this->getDoctrine()->getManager()->remove($m);
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(['status' => 1]);
    }


    /**
     * @Route("/api/r/{id}", name="api.ratio.edit")
     * @Template()
     */
    public function ratioEditAction(Request $request, $id = null)
    {
        /** @var Measure $m */
        $m = $this->getDoctrine()->getRepository('ZantolovBsBundle:Measure')->find($id);
        $m->getChildrenMeasures();
        $allMeasures = $this->getDoctrine()->getManager()->getRepository('ZantolovBsBundle:Measure')->findAll();

        if (empty($m)) {
            throw new EntityNotFoundException();
        }

        if ($request->getMethod() == 'POST') {

            try {
                $data = $request->get('data');

                // Remove previous
                $oldMrs = $m->getChildrenMeasures();
                foreach ($oldMrs as $oldMr) {
                    $this->getDoctrine()->getManager()->remove($oldMr);
                }

                // Add new
                foreach ($data as $mrData) {
                    $mr = new MeasureRatio();
                    $mr->setParentMeasure($m);
                    $mr->setMeasure($this->getDoctrine()->getRepository('ZantolovBsBundle:Measure')->find($mrData['measureId']));
                    $mr->setRatio($mrData['ratio']);
                    $this->getDoctrine()->getManager()->persist($mr);
                    $this->getDoctrine()->getManager()->flush();
                }

                $this->getDoctrine()->getManager()->persist($mr);
                $this->getDoctrine()->getManager()->flush();
                return new JsonResponse(['status' => 1]);
            } catch (\Exception $e) {
                return new JsonResponse(['status' => 0, 'msg' => $e->getMessage()]);

            }
        }

        return compact('m', 'allMeasures');
    }

    /**
     * @Route("/app/m/{id}", name="app.m.details")
     * @Template()
     */
    public function measureDetailsAction($id)
    {
        $measure = $this->getDoctrine()->getRepository('ZantolovBsBundle:Measure')->find($id);
        return new JsonResponse($measure);
    }

}
