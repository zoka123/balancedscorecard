<?php

namespace Zantolov\BsBundle\Controller\CRUD;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Zantolov\AppBundle\Controller\Traits\CrudControllerTrait;
use Zantolov\AppBundle\Controller\Traits\EasyControllerTrait;
use Zantolov\BsBundle\Entity\Measure;
use Zantolov\BsBundle\Form\MeasureType;

/**
 * Class MeasureController
 * @package Zantolov\BsBundle\Controller\CRUD
 */
class MeasureController extends Controller
{
    use EasyControllerTrait;
    use CrudControllerTrait;

    protected function getEntityName()
    {
        return 'ZantolovBsBundle:Measure';
    }

    public static function getCrudId()
    {
        return 'measure';
    }

    protected function getNewEntity()
    {
        return new Measure();
    }

    protected function getCreateFormType()
    {
        return new MeasureType();
    }

}
