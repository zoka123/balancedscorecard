<?php

namespace Zantolov\BsBundle\Controller\CRUD;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Zantolov\AppBundle\Controller\Traits\CrudControllerTrait;
use Zantolov\AppBundle\Controller\Traits\EasyControllerTrait;
use Zantolov\BsBundle\Entity\Measure;
use Zantolov\BsBundle\Entity\MeasureRatio;
use Zantolov\BsBundle\Form\MeasureRatioType;
use Zantolov\BsBundle\Form\MeasureType;

/**
 * Class MeasureRatioController
 * @package Zantolov\BsBundle\Controller\CRUD
 */
class MeasureRatioController extends Controller
{
    use EasyControllerTrait;
    use CrudControllerTrait;

    protected function getEntityName()
    {
        return 'ZantolovBsBundle:MeasureRatio';
    }

    public static function getCrudId()
    {
        return 'measureratio';
    }

    protected function getNewEntity()
    {
        return new MeasureRatio();
    }

    protected function getCreateFormType()
    {
        return new MeasureRatioType();
    }

}
