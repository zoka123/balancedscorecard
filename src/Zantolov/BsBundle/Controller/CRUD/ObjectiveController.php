<?php

namespace Zantolov\BsBundle\Controller\CRUD;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Zantolov\AppBundle\Controller\Traits\CrudControllerTrait;
use Zantolov\AppBundle\Controller\Traits\EasyControllerTrait;
use Zantolov\BsBundle\Entity\Objective;
use Zantolov\BsBundle\Form\ObjectiveType;

/**
 * Class ObjectiveController
 * @package Zantolov\BsBundle\Controller\CRUD
 */
class ObjectiveController extends Controller
{
    use EasyControllerTrait;
    use CrudControllerTrait;

    protected function getEntityName()
    {
        return 'ZantolovBsBundle:Objective';
    }

    public static function getCrudId()
    {
        return 'objective';
    }

    protected function getNewEntity()
    {
        return new Objective();
    }

    protected function getCreateFormType()
    {
        return new ObjectiveType();
    }

}
