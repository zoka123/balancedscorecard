<?php

$builder = new \Zantolov\AppBundle\Service\CrudRouteBuilderService(
    \Zantolov\BsBundle\Controller\CRUD\ObjectiveController::getRoutesConfig(),
    'ZantolovBsBundle:CRUD\Objective'
);


return $builder->buildCrudRouteCollection();