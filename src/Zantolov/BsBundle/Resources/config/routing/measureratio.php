<?php

$builder = new \Zantolov\AppBundle\Service\CrudRouteBuilderService(
    \Zantolov\BsBundle\Controller\CRUD\MeasureRatioController::getRoutesConfig(),
    'ZantolovBsBundle:CRUD\MeasureRatio'
);


return $builder->buildCrudRouteCollection();