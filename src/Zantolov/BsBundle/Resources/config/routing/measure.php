<?php

$builder = new \Zantolov\AppBundle\Service\CrudRouteBuilderService(
    \Zantolov\BsBundle\Controller\CRUD\MeasureController::getRoutesConfig(),
    'ZantolovBsBundle:CRUD\Measure'
);


return $builder->buildCrudRouteCollection();